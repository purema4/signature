export default class CanvasComponent extends HTMLElement {
    constructor() {
        super()

        this.shadow = this.attachShadow({
            mode: 'open'
        });
        this.shadow.innerHTML = `
          <style media="screen">
            :host {
              cursor: pointer;
              display: block;
              width: ${this.getAttribute('fullscreen') == "" ? "100%" : "auto" };
              height: ${this.getAttribute('fullscreen') == "" ? "100%" : "auto" };
            }

            canvas {
              touch-action: none;
            }
          </style>
          <canvas></canvas>
        `;
    }

    connectedCallback() {
        this.canvas = this.shadow.querySelector('canvas');
        this.context = this.canvas.getContext('2d');
        // Update the canvas to fill the space of the component.
        this.canvas.width = this.clientWidth;
        this.canvas.height = this.clientHeight;


        this.init()
        this.loadEvents()
        window.touchCanvas = document.querySelector("#cTouch")
        this.drawLoop()


        //detect when orientation change

        window.addEventListener('orientationchange', () => {});
    }

    init() {
      this.context.strokeStyle = "#222222";
      this.context.lineWidth = 5;
      this.drawing = false;
      this.mousePos = { x:0, y:0 };
      this.lastPos = this.mousePos;


      document.body.addEventListener("touchstart", function (e) {
        if (e.target == window.touchCanvas.canvas) {
          e.preventDefault();
        }
      }, false);
      document.body.addEventListener("touchend", function (e) {
        if (e.target == window.touchCanvas.canvas) {
          e.preventDefault();
        }
      }, false);
      document.body.addEventListener("touchmove", function (e) {
        if (e.target == window.touchCanvas.canvas) {
          e.preventDefault();
        }
      }, false);
    }


    loadEvents() {

      this.canvas.addEventListener("mousedown", (e) => {
        e.preventDefault()
        this.drawing = true;
        this.lastPos = this.getMousePos(e);
        return false
      }, false);

      this.canvas.addEventListener("mouseup",  (e) => {
        e.preventDefault()
        this.drawing = false;
        return false
      }, false);

      this.canvas.addEventListener("mousemove",  (e) => {
        e.preventDefault()
        this.mousePos = this.getMousePos(e);
        return false
      }, false);


      this.canvas.addEventListener("touchstart", (e) => {
        e.preventDefault()
        this.mousePos = this.getTouchPos(e);
        const touch = e.touches[0];
        const mouseEvent = new MouseEvent("mousedown", {
          clientX: touch.clientX,
          clientY: touch.clientY
        });
        this.canvas.dispatchEvent(mouseEvent);
      }, false);
      this.canvas.addEventListener("touchend",  (e) => {
        e.preventDefault()

        const mouseEvent = new MouseEvent("mouseup", {});
        this.canvas.dispatchEvent(mouseEvent);
      }, false);
      this.canvas.addEventListener("touchmove",  (e) => {
        e.preventDefault()

        const touch = e.touches[0];
        const mouseEvent = new MouseEvent("mousemove", {
          clientX: touch.clientX,
          clientY: touch.clientY
        });
        this.canvas.dispatchEvent(mouseEvent);
      }, false);
    }

    drawLoop () {
      requestAnimationFrame(window.touchCanvas.drawLoop);
      window.touchCanvas.renderCanvas();
  }

    getMousePos(mouseEvent) {
      const rect = this.canvas.getBoundingClientRect();
      return {
        x: mouseEvent.clientX - rect.left,
        y: mouseEvent.clientY - rect.top
      };
    }


    getTouchPos(touchEvent) {
      const rect = this.canvas.getBoundingClientRect();
      return {
        x: touchEvent.touches[0].clientX - rect.left,
        y: touchEvent.touches[0].clientY - rect.top
      };
    }

    renderCanvas() {
      if (this.drawing) {
        this.context.moveTo(this.lastPos.x, this.lastPos.y);
        this.context.lineTo(this.mousePos.x, this.mousePos.y);
        this.context.stroke();
        this.lastPos = this.mousePos;
      }
    }
}