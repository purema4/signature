const mongoose = require('mongoose');

const {
    URL,
    DATABASE
} = process.env

module.exports = () => mongoose.connect(`mongodb://${URL}/${DATABASE}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})