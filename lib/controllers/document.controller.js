//const express = require('express')
const documentModel = require('../models/document.model')

let documentController = function() {}

documentController.findById = (id) => documentModel.findById(id) 
documentController.save = (document) => documentModel.save(document) 

module.exports = documentController