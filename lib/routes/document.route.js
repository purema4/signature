const express = require('express')
const documentController = require('../controllers/document.controller')


const router = express.Router()

router.get('/:id', async (req,res) => {

    try {
        const response = await documentController.findById(req.params.id)
        res.json(response)
    } catch (error) {
        res.json(error)
    }

})
.post('', async (req,res) => {
    const {title} = req.body
    try {
        const response = await documentController.save({title})
        res.json(response)
    } catch (error) {
        res.json(error)
    }
})

module.exports = router