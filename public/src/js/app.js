import canvasComponent from './lib/components/canvas.component.mjs'

window.addEventListener("DOMContentLoaded", (e) => {
    e.preventDefault()

    customElements.define('canvas-touch', canvasComponent)
})


