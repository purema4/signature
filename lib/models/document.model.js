const mongoose = require('mongoose')
const mongooseConnect = require("../helpers/mongoConnect")

mongooseConnect()

const {
    ObjectId
} = mongoose.Types

let documentSchema = new mongoose.Schema({
    title: String
});

let Document = mongoose.model('Document', documentSchema);


class documentModel {
    static save(data) {
        let newDocument = new Document(data);
        return new Promise((resolve, reject) => {
            newDocument.save(function (err, newDocument) {
                if (err) reject(err)
                resolve(newDocument)
            })
        })
    }

    static findById(id) {
        return new Promise((resolve, reject) => {

            //check if ID is valid object id
            if (!ObjectId.isValid(id)) {
                reject("Invalid ID cast")
                return false
            }

            Document.findById(new ObjectId(id), (err, document) => {
                if (err) reject(err)
                resolve(document)
                return false
            })
        })
    }
}

module.exports = documentModel