const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const helmet = require("helmet")
const morgan = require("morgan")
const cors = require("cors")

const defaultRouter = require('./lib/routes/default.route')
const documentRouter = require('./lib/routes/document.route')

const PORT = process.env.PORT || 3000

//helmet pour la sécurité de base
app.use(helmet())

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())


//cors policy
app.use(cors())

//morgan pour du logging
app.use(morgan("combined"))

app.use('/', defaultRouter)
app.use('/document', documentRouter)

app.listen(PORT, () => {
    console.log(`Running on port ${PORT}`)
})